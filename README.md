# VVV Custom site template

This template is based on [custom site template by Varying Vagrant Vagrants](https://github.com/Varying-Vagrant-Vagrants/custom-site-template) and we aim to create any kind of WordPress site through pre-sets.

## Overview

This template will allow you to create a WordPress dev environment using only `vvv-custom.yml`.

The supported environments are:
- A single site
- A subdomain multisite
- A subdirectory multisite

## Changes from the original

This is a custom template for me [Borja LoFe](https://gitlab.com/borjalofe/) so I've added code to install and setup my usual plugins and theme. I'm also working to improve times and customizations through pre-sets.

## Configuration

### The minimum required configuration:

```
my-site:
  repo: https://gitlab.com/borjalofe/custom-site-template
  hosts:
    - my-site.dev
```
| Setting    | Value       |
|------------|-------------|
| Domain     | my-site.dev |
| Site Title | my-site.dev |
| DB Name    | my-site     |
| Site Type  | Single      |
| WP Version | Latest      |

### Minimal configuration with custom domain and WordPress Nightly:

```
my-site:
  repo: https://gitlab.com/borjalofe/custom-site-template
  hosts:
    - foo.dev
  custom:
    wp_version: nightly
```
| Setting    | Value       |
|------------|-------------|
| Domain     | foo.dev     |
| Site Title | foo.dev     |
| DB Name    | my-site     |
| Site Type  | Single      |
| WP Version | Nightly     |

### WordPress Multisite with Subdomains:

```
my-site:
  repo: https://gitlab.com/borjalofe/custom-site-template
  hosts:
    - multisite.dev
    - site1.multisite.dev
    - site2.multisite.dev
  custom:
    wp_type: subdomain
```
| Setting    | Value               |
|------------|---------------------|
| Domain     | multisite.dev       |
| Site Title | multisite.dev       |
| DB Name    | my-site             |
| Site Type  | Subdomain Multisite |
| WP Version | Nightly             |

## Configuration Options

```
hosts:
    - foo.dev
    - bar.dev
    - baz.dev
```
Defines the domains and hosts for VVV to listen on. 
The first domain in this list is your sites primary domain.

```
custom:
    site_title: My Awesome Dev Site
```
Defines the site title to be set upon installing WordPress.

```
custom:
    wp_version: 4.6.4
```
Defines the WordPress version you wish to install.
Valid values are:
- nightly
- latest
- a version number

Older versions of WordPress will not run on PHP7, see this page on [how to change PHP version per site](https://varyingvagrantvagrants.org/docs/en-US/adding-a-new-site/changing-php-version/).

```
custom:
    wp_type: single
```
Defines the type of install you are creating.
Valid values are:
- single
- subdomain
- subdirectory

```
custom:
    db_name: super_secet_db_name
```
Defines the DB name for the installation.

### Base Preset

This is the base preset and it can be used for any kind of website. It's the default preset, so there's no code needed but you can also activate it by code:

```
custom:
  preset: base
```

It includes the following plugins:

- [Beaver Builder](https://wordpress.org/plugins/beaver-builder-lite-version/)
- [Contact Form 7](https://wordpress.org/plugins/contact-form-7/)
- [Contact Form 7 Honeypot](https://wordpress.org/plugins/contact-form-7-honeypot/)
- [Cookies Cat](https://wordpress.org/plugins/cookie-cat/)
- [Elementor](https://wordpress.org/plugins/elementor/)
- [EU Cookie Law](https://wordpress.org/plugins/eu-cookie-law/)
- [iThemes](https://wordpress.org/plugins/better-wp-security/)
- [oik](https://wordpress.org/plugins/oik/) (Required by Cookie Cat)
- [WordPress SEO](https://wordpress.org/plugins/wordpress-seo/)
- [WP Maintenance Mode](https://wordpress.org/plugins/wp-maintenance-mode/)
- [W3 Total Cache](https://wordpress.org/plugins/w3-total-cache/)

I'm using the [Twenty Seventeen](https://wordpress.org/themes/twentyseventeen/) theme for this preset.

### Blog Preset

This is the blog preset and it can be used for WordPress Blogs. You can activate it with the following piece of code:

```
custom:
  preset: blog
```

It includes the following plugins:

- [Beaver Builder](https://wordpress.org/plugins/beaver-builder-lite-version/)
- [Contact Form 7](https://wordpress.org/plugins/contact-form-7/)
- [Contact Form 7 Honeypot](https://wordpress.org/plugins/contact-form-7-honeypot/)
- [Cookies Cat](https://wordpress.org/plugins/cookie-cat/)
- [Disqus](https://wordpress.org/plugins/disqus-comment-system/)
- [Elementor](https://wordpress.org/plugins/elementor/)
- [EU Cookie Law](https://wordpress.org/plugins/eu-cookie-law/)
- [Hype](https://wordpress.org/plugins/buffer-my-post/)
- [iThemes](https://wordpress.org/plugins/better-wp-security/)
- [oik](https://wordpress.org/plugins/oik/) (Required by Cookie Cat)
- [Recent Posts Widget with Thumbnails](https://wordpress.org/plugins/recent-posts-widget-with-thumbnails/)
- [Related Posts for WP](https://wordpress.org/plugins/related-posts-for-wp/)
- [WordPress SEO](https://wordpress.org/plugins/wordpress-seo/)
- [WP Maintenance Mode](https://wordpress.org/plugins/wp-maintenance-mode/)
- [W3 Total Cache](https://wordpress.org/plugins/w3-total-cache/)

I'm using the [Twenty Seventeen](https://wordpress.org/themes/twentyseventeen/) theme for this preset.

### Experimental Added Preset

This is a preset I'm using to test plugins and themes for several case studies. You can activate it with the following piece of code:

```
custom:
  experimental: true
```

It includes the following plugins:

- [Above the Fold Optimization](https://wordpress.org/plugins/above-the-fold-optimization/)
- [Addon Elements for Elementor Page Builder](https://wordpress.org/plugins/addon-elements-for-elementor-page-builder/)
- [Anywhere Elementor](https://wordpress.org/plugins/anywhere-elementor/)
- [AJAX Load More](https://wordpress.org/plugins/ajax-load-more/)
- [Antivirus](https://wordpress.org/plugins/antivirus/)
- [Beaver Builder](https://wordpress.org/plugins/beaver-builder-lite-version/)
- [Beaver Builder Bootstrap Alerts](https://wordpress.org/plugins/bb-bootstrap-alerts/)
- [Beaver Builder Bootstrap Cards](https://wordpress.org/plugins/bb-bootstrap-cards/)
- [Beaver Builder Header Footer](https://wordpress.org/plugins/bb-header-footer/)
- [Bootstrap for Contact Form 7](https://wordpress.org/plugins/bootstrap-for-contact-form-7/)
- [BWP Minify](https://wordpress.org/plugins/bwp-minify/)
- [Caldera Forms](https://wordpress.org/plugins/caldera-forms/)
- [Caldera Forms MetaBox](https://wordpress.org/plugins/caldera-forms-metabox/)
- [Caldera Forms PDF](https://wordpress.org/plugins/caldera-forms-pdf/)
- [Caldera Forms Run Action](https://wordpress.org/plugins/caldera-forms-run-action/)
- [Conditional Fail for Caldera Forms](https://wordpress.org/plugins/conditional-fail-for-caldera-forms/)
- [Contact Form 7 Add Confirm](https://wordpress.org/plugins/contact-form-7-add-confirm/)
- [Contact Form 7 Conditional Fields](https://wordpress.org/plugins/cf7-conditional-fields/)
- [Contact Form 7 Datepicker](https://wordpress.org/plugins/contact-form-7-datepicker/)
- [Contact Form 7 Dynamic Mail To](https://wordpress.org/plugins/contact-form-7-dynamic-mail-to/)
- [Contact Form 7 Mailchimp](https://wordpress.org/plugins/contact-form-7-mailchimp-extension/)
- [Contact Form 7 Multi Step](https://wordpress.org/plugins/cf7-multi-step/)
- [Contact Form 7 Multi Step Module](https://wordpress.org/plugins/contact-form-7-multi-step-module/)
- [Contact Form 7 Paypal Extension](https://wordpress.org/plugins/contact-form-7-paypal-extension/)
- [Contact Form 7 Signature Addon](https://wordpress.org/plugins/contact-form-7-signature-addon/)
- [Contact Form 7 Style](https://wordpress.org/plugins/contact-form-7-style/)
- [Dashboard Welcome for Beaver Builder](https://wordpress.org/plugins/dashboard-welcome-for-beaver-builder/)
- [Defer CSS addon for BWP Minify](https://wordpress.org/plugins/defer-css-addon-for-bwp-minify/)
- [Disqus Conditional Load](https://wordpress.org/plugins/disqus-conditional-load/)
- [DZ Elementor Addon](https://wordpress.org/plugins/dz-elementor-addon/)
- [Elementor Addon Widgets](https://wordpress.org/plugins/elementor-addon-widgets/)
- [Elementor Templater](https://wordpress.org/plugins/elementor-templater/)
- [Easy Testimonials](https://wordpress.org/plugins/easy-testimonials/)
- [EWWW Image Optimizer](https://wordpress.org/plugins/ewww-image-optimizer/)
- [Expandable Row for Beaver Builder](https://wordpress.org/plugins/expandable-row-for-beaver-builder/)
- [Exploit Scanner](https://wordpress.org/plugins/exploit-scanner/)
- [Forms 3rd party integration](https://wordpress.org/plugins/forms-3rdparty-integration/)
- [Full Site Builder for Elementor](https://wordpress.org/plugins/full-site-builder-for-elementor/)
- [Google Pagespeed Insights](https://wordpress.org/plugins/google-pagespeed-insights/)
- [Latch](https://wordpress.org/plugins/latch/)
- [Limit Login Attemps](https://wordpress.org/plugins/limit-login-attempts-reloaded/)
- [MailRelay](https://wordpress.org/plugins/mailrelay/)
- [NavMenu Addon for Elementor](https://wordpress.org/plugins/navmenu-addon-for-elementor/)
- [Plugin Organizer](https://wordpress.org/plugins/plugin-organizer/)
- [PODs](https://wordpress.org/plugins/pods/)
- [PODs SEO](https://wordpress.org/plugins/pods-seo/)
- [PowerPack Addon for Beaver Builder](https://wordpress.org/plugins/powerpack-addon-for-beaver-builder/)
- [Press Elements](https://wordpress.org/plugins/press-elements/)
- [Schema.org for Beaver Builder](https://wordpress.org/plugins/ft-bb-schema/)
- [Send PDF for Contact Form 7](https://wordpress.org/plugins/send-pdf-for-contact-form-7/)
- [SJ Elementor Addon](https://wordpress.org/plugins/sj-elementor-addon/)
- [Slack integration for Caldera Forms](https://wordpress.org/plugins/slack-integration-for-caldera-forms/)
- [Sumo.me](https://wordpress.org/plugins/sumome/)
- [Super Simple Google Analytics](https://wordpress.org/plugins/super-simple-google-analytics/)
- [Template Widget for Beaver Builder](https://wordpress.org/plugins/template-widget-for-beaver-builder/)
- [The Events Calendar](https://wordpress.org/plugins/the-events-calendar/)
- [Timeline for Beaver Builder](https://wordpress.org/plugins/timeline-for-beaver-builder/)
- [Ultimate Addons for Beaver Builder](https://wordpress.org/plugins/ultimate-addons-for-beaver-builder-lite/)
- [Verify Email for Caldera Forms](https://wordpress.org/plugins/verify-email-for-caldera-forms/)
- [Vimeography](https://wordpress.org/plugins/vimeography/)
- [WP Broken Link Status](https://wordpress.org/plugins/wp-link-status/)
- [WP Scanner](https://wordpress.org/plugins/wp-scanner/)
- [WP Flexible Maps](https://wordpress.org/plugins/wp-flexible-map/)
- [WPD Beaver Builder Additions](https://wordpress.org/plugins/wpd-bb-additions/)

### To Do

- Create an eCommerce preset
