#!/usr/bin/env bash
# Provision WordPress Stable

DOMAIN=`get_primary_host "${VVV_SITE_NAME}".dev`
DOMAINS=`get_hosts "${DOMAIN}"`
SITE_TITLE=`get_config_value 'site_title' "${DOMAIN}"`
WP_VERSION=`get_config_value 'wp_version' 'latest'`
WP_TYPE=`get_config_value 'wp_type' "single"`
DB_NAME=`get_config_value 'db_name' "${VVV_SITE_NAME}"`
DB_NAME=${DB_NAME//[\\\/\.\<\>\:\"\'\|\?\!\*-]/}
PRESET=`get_config_value 'preset' "blog"`
EXPERIMENTAL=`get_config_value 'experimental' "false"`

# Make a database, if we don't already have one
echo -e "\nCreating database '${DB_NAME}' (if it's not already there)"
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS ${DB_NAME}"
mysql -u root --password=root -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO wp@localhost IDENTIFIED BY 'wp';"
echo -e "\n DB operations done.\n\n"

# Nginx Logs
mkdir -p ${VVV_PATH_TO_SITE}/log
touch ${VVV_PATH_TO_SITE}/log/error.log
touch ${VVV_PATH_TO_SITE}/log/access.log

# Install and configure the latest stable version of WordPress
if [[ ! -f "${VVV_PATH_TO_SITE}/public_html/wp-load.php" ]]; then
    echo "Downloading WordPress..."
	noroot wp core download --version="${WP_VERSION}"
fi

if [[ ! -f "${VVV_PATH_TO_SITE}/public_html/wp-config.php" ]]; then
  echo "Configuring WordPress Stable..."
  noroot wp core config --dbname="${DB_NAME}" --dbuser=wp --dbpass=wp --quiet --extra-php <<PHP
define( 'WP_DEBUG', true );
PHP
fi

if ! $(noroot wp core is-installed); then
  echo "Installing WordPress Stable..."

  if [ "${WP_TYPE}" = "subdomain" ]; then
    INSTALL_COMMAND="multisite-install --subdomains"
  elif [ "${WP_TYPE}" = "subdirectory" ]; then
    INSTALL_COMMAND="multisite-install"
  else
    INSTALL_COMMAND="install"
  fi

  noroot wp core ${INSTALL_COMMAND} --url="${DOMAIN}" --quiet --title="${SITE_TITLE}" --admin_name=admin --admin_email="admin@local.dev" --admin_password="password"
else
  echo "Updating WordPress Stable..."
  cd ${VVV_PATH_TO_SITE}/public_html
  noroot wp core update --version="${WP_VERSION}"
fi

if [ "${PRESET}" = "ecommerce" ]; then
  noroot wp plugin install --activate autoptimize beaver-builder-lite-version better-wp-security buffer-my-post contact-form-7 contact-form-7-honeypot cookie-cat disqus-comment-system elementor eu-cookie-law oik recent-posts-widget-with-thumbnails related-posts-for-wp tweet-old-post woocommerce wordpress-seo wp-maintenance-mode w3-total-cache
else
  noroot wp plugin install --activate autoptimize beaver-builder-lite-version better-wp-security buffer-my-post contact-form-7 contact-form-7-honeypot cookie-cat disqus-comment-system elementor eu-cookie-law oik recent-posts-widget-with-thumbnails related-posts-for-wp tweet-old-post wordpress-seo wp-maintenance-mode w3-total-cache
fi

if [ "${EXPERIMENTAL}" = "true" ]; then
  noroot wp plugin install pods pods-seo caldera-forms caldera-forms-run-action caldera-form-metabox conditional-fail-for-caldera-forms forms-3rdparty-integration slack-integration-for-caldera-forms verify-email-for-caldera-forms caldera-forms-pdf addon-elements-for-elementor-page-builder anywhere-elementor sj-elementor-addon dz-elementor-addon elementor-templater press-elements full-site-builder-for-elementor navmenu-addon-for-elementor elementor-addon-widgets beaver-builder-lite-version bb-header-footer timeline-for-beaver-builder dashboard-welcome-for-beaver-builder ultimate-addons-for-beaver-builder-lite bb-bootstrap-cards ft-bb-schema bb-bootstrap-alerts powerpack-addon-for-beaver-builder expandable-row-for-beaver-builder template-widget-for-beaver-builder wpd-bb-additions ajax-load-more cf7-conditional-fields send-pdf-for-contact-form-7 contact-form-7-add-confirm contact-form-7-style bootstrap-for-contact-form-7 contact-form-7-mailchimp-extension contact-form-7-datepicker contact-form-7-signature-addon contact-form-7-multi-step-module cf7-multi-step contact-form-7-dynamic-mail-to contact-form-7-paypal-extension disqus-conditional-load the-events-calendar vimeography wp-link-status wp-scanner antivirus exploit-scanner latch limit-login-attempts-reloaded easy-testimonials super-simple-google-analytics mailrelay sumome wp-flexible-map above-the-fold-optimization bwp-minify defer-css-addon-for-bwp-minify ewww-image-optimizer google-pagespeed-insights plugin-organizer
fi

cp -f "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf.tmpl" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"
sed -i "s#{{DOMAINS_HERE}}#${DOMAINS}#" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"
